class BookmarksController < ApplicationController
  def index
    @bookmarks = Bookmark.search(params[:q]).order(:created_at)
  end

  def new
    @bookmark = Bookmark.new
  end

  def create
    @bookmark = BookmarkCreator.new(bookmark_params).create

    if @bookmark.persisted?
      redirect_to :bookmarks
    else
      render :new
    end
  end

  def edit
    @bookmark = Bookmark.find(params[:id])
  end

  def update
    @bookmark = BookmarkUpdater.new(
      Bookmark.find(params[:id]),
      bookmark_params
    ).update

    if @bookmark.valid?
      redirect_to :bookmarks
    else
      render :edit
    end
  end

  private

  def bookmark_params
    params.require(:bookmark).permit(:title, :url, :short_url, :inline_tags)
  end
end
