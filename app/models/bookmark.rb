class Bookmark < ApplicationRecord
  belongs_to :site
  has_and_belongs_to_many :tags

  validates :site, :url, :title, presence: true

  scope :search, ->(q) {
    joins(%Q{
      JOIN (
        SELECT
          bookmarks.id,

          (
            to_tsvector(bookmarks.title) ||
            to_tsvector(bookmarks.url) ||
            to_tsvector(COALESCE(bookmarks.short_url, '')) ||
            to_tsvector(
              COALESCE(string_agg(tags.name, ' '), '')
            )
          )  as document
        FROM bookmarks
        LEFT JOIN bookmarks_tags ON bookmarks_tags.bookmark_id = bookmarks.id
        LEFT JOIN tags ON tags.id = bookmarks_tags.tag_id
        GROUP BY bookmarks.id
      ) terms ON terms.id = bookmarks.id
    }).where('terms.document @@ ?', q).select('DISTINCT bookmarks.*')
  }

  def inline_tags
    tags.order(:name).map(&:name).join(',')
  end

  def inline_tags=(tags)
    self.tags = tags.split(',').map do |name|
      Tag.find_or_create_by(name: name)
    end
  end
end
