require 'uri'

class BookmarkCreator
  def initialize(params)
    @bookmark = Bookmark.new(params)
  end

  def create
    Bookmark.transaction do
      site = find_or_create_site
      @bookmark.site = site

      raise ActiveRecord::Rollback unless @bookmark.save
    end

    @bookmark
  end

  private

  def find_or_create_site
    uri = URI.parse(@bookmark.url)

    site_url = "#{uri.scheme}://#{uri.host}"

    Site.find_or_create_by(url: site_url)
  end
end
