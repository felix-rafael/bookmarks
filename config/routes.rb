Rails.application.routes.draw do
  root to: 'bookmarks#index'

  resources :bookmarks, only: %i[index new create edit update]
end
