class CreateSites < ActiveRecord::Migration[5.1]
  def change
    create_table :sites do |t|
      t.string :url

      t.timestamps

      t.index :url, unique: true
    end
  end
end
