class CreateBookmarksTags < ActiveRecord::Migration[5.1]
  def change
    create_table :bookmarks_tags, id: false do |t|
      t.belongs_to :bookmark, foreign_key: true
      t.belongs_to :tag, foreign_key: true
    end

    add_index :bookmarks_tags, [:bookmark_id, :tag_id], unique: true
  end
end
