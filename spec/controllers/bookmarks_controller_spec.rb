require 'rails_helper'

RSpec.describe BookmarksController do
  fixtures :sites, :bookmarks

  describe 'GET #index' do
    it 'returns a list of all bookmarks' do
      get :index

      assigns(:bookmarks).size == 2
    end
  end

  describe 'POST #create' do
    it 'creates a new bookmark' do
      expect {
        post :create, params: { bookmark: { title: 'something', url: 'http://crazyurl.com/something' } }
      }.to change(Bookmark, :count).by(1)

      expect(response).to redirect_to bookmarks_path
    end

    it 'renders the new template when error happen' do
      post :create, params: { bookmark: { url: 'http://crazyurl.com/something' } }

      expect(response).to render_template('new')
    end
  end

  describe 'PATCH #update' do
    let(:dogs) { bookmarks(:reddit_dogs) }

    it 'updates an existing bookmark' do
      expect {
        patch :update, params: { id: dogs.id, bookmark: { title: 'something', url: 'http://crazyurl.com/something' } }
      }.to change(Site, :count).by(1)

      expect(response).to redirect_to bookmarks_path
      dogs.reload
      expect(dogs.title).to eq('something')
      expect(dogs.url).to eq('http://crazyurl.com/something')
    end

    it 'renders the edit template when error happen' do
      patch :update, params: { id: dogs.id, bookmark: { title: '' } }

      expect(response).to render_template('edit')
    end
  end
end
