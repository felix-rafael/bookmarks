require 'rails_helper'

describe Bookmark do
  describe '.search' do
    fixtures :sites, :bookmarks, :tags

    it 'searches by the title' do
      search_result = described_class.search('dog')

      expect(search_result.size).to eq(1)
      expect(search_result.first).to eq(bookmarks(:reddit_dogs))
    end

    it 'searches by url' do
      search_result = described_class.search('youtube.com')

      expect(search_result.size).to eq(1)
      expect(search_result.first).to eq(bookmarks(:youtube_video))
    end

    it 'searches by tag' do
      search_result = described_class.search('random')

      expect(search_result.size).to eq(1)
      expect(search_result.first).to eq(bookmarks(:youtube_video))
    end
  end

  describe 'tags' do
    fixtures :sites, :bookmarks

    it 'generates new tags but not old ones' do
      Tag.create!(name: 'something')

      dogs = bookmarks(:reddit_dogs)
      expect {
        dogs.inline_tags = 'something,dogs,reddit'
      }.to change(Tag, :count).by(2)
    end

    it 'updates the existing tags' do
      dogs = bookmarks(:reddit_dogs)
      dogs.inline_tags = 'something,dogs,reddit'
      dogs.save

      expect(dogs.reload.tags.size).to eq(3)
      dogs.inline_tags = 'reddit,dogs'
      dogs.save

      expect(dogs.reload.tags.size).to eq(2)
      expect(dogs.inline_tags).to eq('dogs,reddit')
    end
  end
end
