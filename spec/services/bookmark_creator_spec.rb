require 'rails_helper'

RSpec.describe BookmarkCreator do
  fixtures :sites

  describe 'creating a new bookmark' do
    it 'creates a new site according to the bookmark url' do
      bookmark_creator = described_class.new({
        title: 'something',
        url: 'http://google.com/something',
        short_url: 'http://s.ly/something'
      })

      expect {
        bookmark = bookmark_creator.create

        expect(bookmark.title).to eq('something')
        expect(bookmark.url).to eq('http://google.com/something')
        expect(bookmark.short_url).to eq('http://s.ly/something')
      }.to change(Site, :count).by(1)
    end

    it 'creates a new site even if the bookmark has no path' do
      bookmark_creator = described_class.new({
        title: 'something',
        url: 'http://something.com'
      })

      expect {
        bookmark = bookmark_creator.create

        expect(bookmark.title).to eq('something')
        expect(bookmark.url).to eq('http://something.com')
        expect(bookmark.short_url).to eq(nil)
      }.to change(Site, :count).by(1)
    end

    it 'does not create a new site if it already exists' do
      bookmark_creator = described_class.new({
        title: 'something',
        url: 'http://reddit.com/something'
      })

      expect {
        bookmark = bookmark_creator.create

        expect(bookmark.title).to eq('something')
        expect(bookmark.url).to eq('http://reddit.com/something')
      }.to_not change(Site, :count)
    end

    it 'does not create the site if the bookmark is invalid' do
      bookmark_creator = described_class.new({
        url: 'http://reddit.com/something'
      })

      expect {
        expect(bookmark_creator.create).to be_new_record
      }.to_not change(Site, :count)
    end
  end
end
