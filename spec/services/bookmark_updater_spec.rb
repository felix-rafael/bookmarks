require 'rails_helper'

RSpec.describe BookmarkUpdater do
  fixtures :sites, :bookmarks

  describe 'updating a new bookmark' do
    it 'creates a new site according to the bookmark url' do
      bookmark_updater = described_class.new(
        bookmarks(:reddit_dogs),
        {
          title: 'something',
          url: 'http://google.com/something',
          short_url: 'http://s.ly/something'
        }
      )

      expect {
        bookmark = bookmark_updater.update

        expect(bookmark.title).to eq('something')
        expect(bookmark.url).to eq('http://google.com/something')
        expect(bookmark.short_url).to eq('http://s.ly/something')
      }.to change(Site, :count).by(1)
    end

    it 'creates a new site even if the bookmark has no path' do
      bookmark_updater = described_class.new(
        bookmarks(:reddit_dogs),
        {
          title: 'something',
          url: 'http://something.com',
          short_url: ''
        }
      )

      expect {
        bookmark = bookmark_updater.update

        expect(bookmark.title).to eq('something')
        expect(bookmark.url).to eq('http://something.com')
        expect(bookmark.short_url).to eq('')
      }.to change(Site, :count).by(1)
    end

    it 'does not create a new site if it already exists' do
      bookmark_updater = described_class.new(
        bookmarks(:reddit_dogs),
        {
          url: 'http://reddit.com/something',
        }
      )

      expect {
        bookmark = bookmark_updater.update

        expect(bookmark.title).to eq('Dogs')
        expect(bookmark.url).to eq('http://reddit.com/something')
      }.to_not change(Site, :count)
    end

    it 'does not create the site if the bookmark is invalid' do
      bookmark_updater = described_class.new(
        bookmarks(:reddit_dogs),
        {
          title: ''
        }
      )

      expect {
        expect(bookmark_updater.update).to eq(bookmarks(:reddit_dogs))
      }.to_not change(Site, :count)
    end
  end
end
